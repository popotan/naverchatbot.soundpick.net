#-*- coding: utf-8 -*-
class QueryMaker(object):
	"""docstring for QueryMaker"""

	positive_adjective_list = [
		'좋', '좋은', '편한', '빵빵', '쎈', '한', "쎈", '강', '있는', '있'
	]
	negative_adjective_list = [
		'안좋', '안', '나쁜', '나쁘', '않은', '싫지', '불편하지', '하지','강하지', '약한', '약하', '약하지', '좋지', '없', '없는'
	]
	parallel_adjective_list = [
		'보다', '비슷한', '만큼', '같은'
	]

	def __init__(self, target_keyword_list):
		self.target_keyword_list = target_keyword_list

	def stem_analysis(self):
		repr_keyword_list = []
		def is_category_word(target):
			if target in ['이어폰', '헤드폰', '헤드셋', '이어셋']:
				return True
			else: return False

		def role_check(target, need_noun_check):
			have_to_check = ['Josa', 'verbPrefix', 'Noun', 'Adjective', 'Verb']
			was_noun = False
			if need_noun_check:
				have_to_check = have_to_check + ['Noun']
				if target['typeof'] == 'Noun': was_noun = True
			if target['typeof'] in have_to_check:
				if target['word'] in self.positive_adjective_list: return 'positive', was_noun
				elif target['word'] in self.negative_adjective_list: return 'negative', was_noun
				elif target['word'] in self.parallel_adjective_list: return 'parallel', was_noun
				else: return 'skip', was_noun
			else: return 'end', was_noun

		def is_next_word_negative(index2):
			is_negative = False
			target_word = self.target_keyword_list[index2]
			flag_check_noun = True
			was_noun_index = -1
			for idx, x in enumerate(self.target_keyword_list[index2:]):
				role_type, was_noun = role_check(x, flag_check_noun)
				print(role_type)
				if was_noun: 
					flag_check_noun = False
					was_noun_index = idx
				if role_type == 'positive': break
				elif role_type == 'negative': is_negative = not is_negative
				elif role_type == 'end': break
			return is_negative, was_noun_index

		skip_target_index = [-1]
		for index, word in enumerate(self.target_keyword_list):
			if index not in skip_target_index:
				if word['typeof'] == 'Noun' or word['typeof'] == 'Alpha':
					is_negative = False
					if index + 1 < len(self.target_keyword_list):
						is_negative, skip_noun_index = is_next_word_negative(index + 1)
						if skip_noun_index > -1:
							skip_target_index.append(skip_noun_index)
					repr_keyword_list.append({'word' : word['word'], 'is_negative' : is_negative})

		return repr_keyword_list