#-*- coding: utf-8 -*-
import json
import urllib.parse
from urllib.parse import urlencode
import urllib.request
import re

class KeywordResolver(object):
	"""docstring for KeywordResolver"""

	to_regex_url = 'https://open-korean-text.herokuapp.com/normalize?'
	to_tokenize_url = 'https://open-korean-text.herokuapp.com/tokenize?'
	to_stem_url = 'https://open-korean-text.herokuapp.com/stem?'
	to_extract_url = 'https://open-korean-text.herokuapp.com/extractPhrases?'

	def __init__(self, target_text):
		parameters = {'text': target_text}
		self.target_text = self.parse_regex_text(urlencode(parameters))

	def remove_atypical_char(target_text):
		target_text = target_text.replace("(","")
		target_text = target_text.replace(")","")
		target_text = target_text.replace("[", "")
		target_text = target_text.replace("]", "")
		return target_text

	def parse_regex_text(self, target_text):
		reqURL = self.to_regex_url + target_text
		result = self.request_from(reqURL)
		return result['strings']

	def parse_tokenized_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_tokenize_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result

	def parse_stem_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_stem_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result

	def parse_extract_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_regex_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result['phrases']

	def split_info(self, target):
		s1 = target.split('(')
		keyword = s1[0]
		s2 = s1[1].split(':')
		target_type = s2[0]
		s3 = s2[1].replace(' ', '').replace(')', '').split(',')

		result = {
			"word" : keyword,
			"typeof" : target_type,
			"index" : [int(s3[0]), int(s3[1])]
		}
		return result

	def request_from(self, reqURL):
		req = urllib.request.Request(reqURL)
		json_result = str(urllib.request.urlopen(req).read().decode("utf-8"))
		return json.loads(json_result)