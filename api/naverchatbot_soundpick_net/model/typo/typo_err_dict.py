# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, asc, desc
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy
from flask import session

from naverchatbot_soundpick_net import db

from datetime import datetime, timedelta

class typo_err_dict_orm(db.Model):
    __bind_key__ = 'chatbot'
    __tablename__ = 'typo_err_dict'
    id = Column(Integer,primary_key=True,unique=True)
    typo = Column(String(100))
    origin = Column(String(100))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))