# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, asc, desc
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy
from flask import session

from naverchatbot_soundpick_net import db

from datetime import datetime, timedelta

class entity_orm(db.Model):
    __bind_key__ = 'chatbot'
    __tablename__ = 'entity'
    id = Column(Integer,primary_key=True,unique=True)
    intent_id = Column(Integer, ForeignKey("intent.id"))
    tag = Column(String(200))
    converse = Column(String(2000))
    sequence = Column(Integer)
    is_essential = Column(Boolean, default=False)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))