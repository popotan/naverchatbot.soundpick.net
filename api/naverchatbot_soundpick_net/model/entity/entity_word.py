# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, asc, desc
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy
from flask import session

from naverchatbot_soundpick_net import db

from datetime import datetime, timedelta

class entity_word_orm(db.Model):
    __bind_key__ = 'chatbot'
    __tablename__ = 'entity_Word'
    id = Column(Integer,primary_key=True,unique=True)
    entity_id = Column(Integer, ForeignKey("entity.id"))
    word = Column(String(200))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))