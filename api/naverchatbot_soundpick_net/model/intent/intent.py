# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey, asc, desc
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy
from flask import session

from naverchatbot_soundpick_net import db

from datetime import datetime, timedelta

from ..entity.entity import entity_orm

class intent_orm(db.Model):
    __bind_key__ = 'chatbot'
    __tablename__ = 'intent'
    id = Column(Integer,primary_key=True,unique=True)
    tag = Column(String(200))
    button_name = Column(String(2000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    entities = relationship(entity_orm, backref="entity", lazy="dynamic")