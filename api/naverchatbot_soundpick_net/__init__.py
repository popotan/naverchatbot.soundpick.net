from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from naverchatbot_soundpick_net.config import database
from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'naverchatbot_soundpick_net'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

@app.route('/static/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	return send_from_directory('static/', path)

@app.before_request
def _before_request_():
    print(request)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route('/', methods=['GET'])
def index():
    return '', 200

@app.route('/ping', methods=['GET'])
def ping_test():
    return 'ping', 200

from naverchatbot_soundpick_net.controller.receive import receive

app.register_blueprint(receive, url_prefix='/api/receive')