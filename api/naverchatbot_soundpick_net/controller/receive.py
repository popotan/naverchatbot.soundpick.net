from flask import Blueprint, render_template, jsonify, request, session, redirect

import urllib.parse
import urllib.request

from naverchatbot_soundpick_net import app
import os, calendar
from datetime import datetime, timedelta

receive = Blueprint('receive', __name__)

RESPONSE_MASSEGE = {
    "event": "send",
    "textContent": {
        "text": "hello world"
        }
    }

@receive.route('/naverchatbot', methods=['GET', 'POST', 'PUT'])
def aaa():
    msg = request.get_json(force=True)
    print(msg)
    if request.method == 'POST' and request.get_json(force=True)['user'] == 'IasPFTnkzGcrQwsZx1KPTg':
        return receive_from_naverchatbot()
    else:
        return '', 200

def receive_from_naverchatbot():
    msg = request.get_json(force=True)
    print(msg)
    if msg['event'] == 'send':
        return jsonify(RESPONSE_MASSEGE)
    elif msg['event'] == 'open':
        if msg['options']['inflow'] == 'list':
            pass
        elif msg['options']['inflow'] == 'button':
            pass
        else:
            pass
    elif msg['event'] == 'friend':
        if msg['options']['set'] == 'on':
            pass
        else:
            pass

@receive.route('/naverchatbot/alert', methods=['GET', 'POST', 'PUT'])
def receive_alert_from_naverchatbot():
    msg = request.get_json(force=True)
    print(msg)
    return '', 200

def send_message_selfish(user, text):
    data = {"event" : "send", "user" : user, "textContent" : {"text" : text}}
    url = 'https://gw.talk.naver.com/chatbot/v1/event'
    req = urllib.request.Request(url, method='POST')
    req.add_header("Authorization","8MYs9mekRGehTdUqv9U3")
    result = urllib.request.urlopen(req).read().decode('utf8')
    return result