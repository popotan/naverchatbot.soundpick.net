import { browser, element, by } from 'protractor';

export class Naverchatbot.Soundpick.NetPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
