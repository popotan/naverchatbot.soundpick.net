import { Naverchatbot.Soundpick.NetPage } from './app.po';

describe('naverchatbot.soundpick.net App', () => {
  let page: Naverchatbot.Soundpick.NetPage;

  beforeEach(() => {
    page = new Naverchatbot.Soundpick.NetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
